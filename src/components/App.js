import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch,
} from 'react-router-dom';
import { ThemeProvider } from 'styled-components';
import { Provider } from 'react-redux';

import axios from 'axios';
import OrderForm from './OrderForm';
import Board from './Board';
import LoginForm from './Login/LoginForm';
import { theme, SlatInner, SlatOuter } from '../common';
import { Error404, PrivateRoute, Nav } from '../common/components';
import { store } from '../store';

axios.defaults.baseURL = 'https://pupusa-comanda.herokuapp.com/api/private';

const App = () => (
  <Provider store={store}>
    <ThemeProvider theme={theme}>
      <SlatOuter>
        <SlatInner>
          <Router>
            <div>
              <Nav />
              <Switch>
                <PrivateRoute
                  exact
                  path="/"
                  component={Board}
                />
                <PrivateRoute
                  exact
                  path="/order/:orderId?"
                  component={OrderForm}
                />
                <Route exact path="/login" component={LoginForm} />
                <Route component={Error404} />
              </Switch>
            </div>
          </Router>
        </SlatInner>
      </SlatOuter>
    </ThemeProvider>
  </Provider>
);

export default App;
