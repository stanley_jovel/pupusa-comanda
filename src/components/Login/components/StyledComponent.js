import styled from 'styled-components';

export const LoginContainer = styled.div`
padding: 20px;
background-color: #ebebeb;
text-align: center;
margin: 5% 30% ;
`;

export const InputLogin = styled.input`
padding: 7px;
border: 1px solid #333;
border-radius: 3px;
& :hover{
border: 1px solid #b29772;
}
width: 40%;
`;

export const LabelLogin = styled.label`
text-align: right;
padding-right: 20px;
width: 20%;
`;

export const FormGroup = styled.div`
display: flex;
justify-content: center;
padding: 10px;
`;

