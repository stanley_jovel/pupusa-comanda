import React from 'react';
import PropTypes from 'prop-types';

import { InputLogin, LabelLogin, FormGroup } from './StyledComponent';

export const LoginField = ({
  changeValue, typeField, nameField, labelName,
}) => (
  <FormGroup>
    <LabelLogin htmlFor={nameField}>{labelName}</LabelLogin>
    <InputLogin
      onChange={changeValue}
      type={typeField}
      name={nameField}
    />
  </FormGroup>
);

LoginField.propTypes = {
  typeField: PropTypes.string.isRequired,
  changeValue: PropTypes.func.isRequired,
  nameField: PropTypes.string.isRequired,
  labelName: PropTypes.string.isRequired,
};
