import React, { Component } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router';

import { LoginField } from './components/LoginField';
import { LoginContainer } from './components/StyledComponent';
import { LinkButton, LinkButtonWrapper } from '../../common';
import { loginActions } from '../../actions';

class LoginForm extends Component {
  static propTypes = {
    dispatchPostAuth: PropTypes.func.isRequired,
    history: PropTypes.shape({
      push: PropTypes.func,
    }).isRequired,
    location: PropTypes.shape({
      state: PropTypes.shape({
        from: PropTypes.string,
      }),
    }).isRequired,
    isLoggedIn: PropTypes.bool.isRequired,
  }

  state = {
    login: {
      username: '',
      password: '',
    },
  }

  postAuth = (event) => {
    event.preventDefault();
    const loginToPost = this.state.login;
    const {
      dispatchPostAuth, history,
    } = this.props;
    dispatchPostAuth(loginToPost, history.push);
  }

  changeValue = (e) => {
    e.persist();
    this.setState(prevState => ({
      login: _.set(
        prevState.login,
        `${e.target.name}`,
        e.target.value,
      ),
    }));
  }

  render() {
    const { from } =
    this.props.location.state ||
      { from: { pathname: '/' } };

    if (this.props.isLoggedIn) {
      return <Redirect to={from} />;
    }
    return (
      <LoginContainer>
        <h1>Login</h1>
        <LoginField
          changeValue={this.changeValue}
          typeField="text"
          nameField="username"
          labelName="Usuario"
        />
        <LoginField
          changeValue={this.changeValue}
          typeField="password"
          nameField="password"
          labelName="Contraseña"
        />
        <LinkButtonWrapper>
          <LinkButton to="/" onClick={this.postAuth}>Entrar</LinkButton>
        </LinkButtonWrapper>
      </LoginContainer>
    );
  }
}

const mapStateProps = reduxState => ({
  isLoggedIn: reduxState.login.isLoggedIn,
});

const mapDispatchToProps = {
  dispatchPostAuth: loginActions.postAuth,
};

export default connect(mapStateProps, mapDispatchToProps)(LoginForm);
