import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { OrderWrapper, OrderHeader, OrderBody, Quantity } from './StyledComponents';
import { OrderType } from '../../../common';
import { orderActions } from '../../../actions';

export const TicketComponent = ({ order, dispatchDeleteOrder }) => (
  <OrderWrapper>
    <OrderHeader>Order #{order.orderNumber}</OrderHeader>
    <OrderBody>
      <div>Arroz</div>
      <ul>
        <li>Queso: <Quantity>{order.arroz.queso}</Quantity></li>
        <li>Frijol Con Queso: <Quantity>{order.arroz.frijolQueso}</Quantity></li>
        <li>Revueltas: <Quantity>{order.arroz.revuelta}</Quantity></li>
      </ul>
      <div>Maiz</div>
      <ul>
        <li>Queso: <Quantity>{order.maiz.queso}</Quantity></li>
        <li>Frijol Con Queso: <Quantity>{order.maiz.frijolQueso}</Quantity></li>
        <li>Revueltas: <Quantity>{order.maiz.revuelta}</Quantity></li>
      </ul>
      <Link
        href={`order/${order._id}`}
        to={`order/${order._id}`}
      >
      Editar
      </Link>
      <button onClick={() => dispatchDeleteOrder(order._id)}>
      Borrar
      </button>
    </OrderBody>
  </OrderWrapper>
);

TicketComponent.propTypes = {
  order: OrderType.isRequired,
  dispatchDeleteOrder: PropTypes.func.isRequired,
};

const mapDispacthToProps = {
  dispatchDeleteOrder: orderActions.deleteOrder,
};


export const Ticket = connect(null, mapDispacthToProps)(TicketComponent);
