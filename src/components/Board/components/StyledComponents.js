import styled from 'styled-components';

export const BoardWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-evenly;
  align-items: center;
  `;

export const OrderWrapper = styled.div`
  width: 300px;
  height: 300px;
  margin: 20px;
  border-radius: 10px;
  box-sizing: border-box;
  color: ${({ theme }) => theme.brown};
  display: flex;
  flex-flow: column;
  justify-content: space-around;
  align-items: center;
  border: 1px solid ${({ theme }) => theme.lightGray2};
  background: white;
  background: linear-gradient(0, ${({ theme }) => theme.lightBlue} 0%, white 8%) 0 57px;
  background-size: 100% 30px;
`;

export const OrderHeader = styled.div`
  width: 90%;
  font-weight: 600;
  font-size: 18px;
  font-size: 25px;
  text-align: center;
  border-bottom: solid;
  text-transform: uppercase;
`;

export const OrderBody = styled.div`
  width: 80%;
  font-size: 16px;
`;

export const Quantity = styled.span``;

