import styled from 'styled-components';

import { SlatInner } from '../../../common';

export const FormWrapper = SlatInner.extend`
  display: flex;
  flex-direction: column;
  align-content: center;
  margin: 0 auto;
  color: ${({ theme }) => theme.brown};
`;

export const Row = styled.div`
  height: 60px;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  border-bottom: 0.5px solid ${({ theme }) => theme.brown};
  &:hover {
    background-color: ${({ theme }) => theme.lightGray};
  }
`;

export const HeaderRow = Row.extend`
  background-color: ${props => props.theme.gray};
  font-size: 20px;
  font-weight: 600;
  text-transform: uppercase;
  &:hover {
    background-color: ${props => props.theme.gray};
  }
`;

export const Cell = styled.div`
  width: 30%;
  text-align: center;
`;

export const FlavourCell = styled.div`
  width: 40%;
  text-align: left;
`;

export const CounterWrapper = styled.div`
  display: flex;
  justify-content: space-evenly;
  align-content: center;
`;

export const CounterButton = styled.button`
  height: 30px;
  width: 30px;
  border-radius: 30px;
  background-color: ${props => props.theme.gray};
  font-size: 20px;
  font-weight: bold;
`;
