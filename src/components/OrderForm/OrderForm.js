import React, { Component } from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import {
  Counter, FormWrapper, Row,
  Cell, HeaderRow, FlavourCell,
} from './components';
import { LinkButton, LinkButtonWrapper, OrderType } from '../../common';
import { orderActions, appActions } from '../../actions';

class OrderForm extends Component {
  static defaultProps = {
    errorMessage: '',
    order: {
      _id: '',
      orderNumber: 0,
      arroz: {
        queso: 0,
        frijolQueso: 0,
        revuelta: 0,
      },
      maiz: {
        queso: 0,
        frijolQueso: 0,
        revuelta: 0,
      },
    },
  }

  static propTypes = {
    dispatchPostOrder: PropTypes.func.isRequired,
    dispatchUpdateOrder: PropTypes.func.isRequired,
    dispatchGetOrder: PropTypes.func.isRequired,
    errorMessage: PropTypes.string,
    dispatchDeleteMessage: PropTypes.func.isRequired,
    history: PropTypes.shape({
      push: PropTypes.func,
    }).isRequired,
    order: OrderType,
    isEditMode: PropTypes.bool.isRequired,
    orderId: PropTypes.string.isRequired,
  }

  state = {
    order: this.props.order,
  }

  componentDidMount() {
    if (this.props.orderId) {
      this.props.dispatchGetOrder(this.props.orderId)
        .then((response) => {
          if (!response.isPending) {
            this.setState({
              order: this.props.order,
            });
          }
        });
    }
  }

  componentWillUnmount() {
    this.props.dispatchDeleteMessage();
  }

  add = (masa, tipo) => {
    this.setState(prevState => ({
      order: _.set(
        prevState.order,
        `${masa}.${tipo}`,
        prevState.order[masa][tipo] + 1,
      ),
    }));
  }

  subtract = (masa, tipo) => {
    this.setState(prevState => ({
      order: _.set(
        prevState.order,
        `${masa}.${tipo}`,
        Math.max(0, prevState.order[masa][tipo] - 1),
      ),
    }));
  }

  addOrder = (event) => {
    event.preventDefault();
    const orderToPost = this.state.order;
    const {
      dispatchUpdateOrder, dispatchPostOrder,
      history, isEditMode,
    } = this.props;

    if (isEditMode) {
      dispatchUpdateOrder(orderToPost, history.push);
    } else {
      dispatchPostOrder(orderToPost, history.push);
    }
  }

  render() {
    if (this.props.order.isLoading) {
      return 'Cargando...';
    }
    return (
      <FormWrapper>
        <LinkButtonWrapper alignment="left">
          <LinkButton to="/">Board</LinkButton>
        </LinkButtonWrapper>
        <h1>{this.props.errorMessage}</h1>
        <HeaderRow>
          <Cell />
          <Cell className="example">Arroz</Cell>
          <Cell>Maiz</Cell>
        </HeaderRow>
        <Row>
          <FlavourCell> Queso </FlavourCell>
          <Cell>
            <Counter
              count={this.state.order.arroz.queso}
              add={() => this.add('arroz', 'queso')}
              subtract={() => this.subtract('arroz', 'queso')}
            />
          </Cell>
          <Cell>
            <Counter
              count={this.state.order.maiz.queso}
              add={() => this.add('maiz', 'queso')}
              subtract={() => this.subtract('maiz', 'queso')}
            />
          </Cell>
        </Row>
        <Row>
          <FlavourCell>Frijol Con Queso</FlavourCell>
          <Cell>
            <Counter
              count={this.state.order.arroz.frijolQueso}
              add={() => this.add('arroz', 'frijolQueso')}
              subtract={() => this.subtract('arroz', 'frijolQueso')}
            />
          </Cell>
          <Cell>
            <Counter
              count={this.state.order.maiz.frijolQueso}
              add={() => this.add('maiz', 'frijolQueso')}
              subtract={() => this.subtract('maiz', 'frijolQueso')}
            />
          </Cell>
        </Row>
        <Row>
          <FlavourCell>Revuelta</FlavourCell>
          <Cell>
            <Counter
              count={this.state.order.arroz.revuelta}
              add={() => this.add('arroz', 'revuelta')}
              subtract={() => this.subtract('arroz', 'revuelta')}
            />
          </Cell>
          <Cell>
            <Counter
              count={this.state.order.maiz.revuelta}
              add={() => this.add('maiz', 'revuelta')}
              subtract={() => this.subtract('maiz', 'revuelta')}
            />
          </Cell>
        </Row>
        <LinkButtonWrapper>
          <LinkButton
            to="/"
            onClick={this.addOrder}
          >
            {
             this.props.isEditMode ? 'Edit ' : 'Add '
              // (() => {
              //   if (this.props.isEditMode) {
              //     return 'Edit ';
              //   }
              //   return 'Add ';
              // })()
            }
            Order
          </LinkButton>
        </LinkButtonWrapper>
      </FormWrapper>
    );
  }
}

// const mapDispatchToProps = dispatch => ({
//   dispatchAddOrder: () => dispatch(orderActions.addOrder()),
// });

// const mapDispatchToProps = (dispatch) => {
//   const boundActionCreators = bindAtionCreators({
//     order: orderActions.addOrder,
//   });
//   return boundActionCreators;
// };

const mapStateProps = (reduxState, ownProps) => {
  const orderId = _.get(ownProps, 'match.params.orderId');

  let order;
  if (orderId) {
    order = reduxState.orders[orderId];
  }
  return {
    order,
    errorMessage: reduxState.app.errorMessage,
    isEditMode: orderId !== undefined,
    orderId,
  };
};

const mapDispatchToProps = {
  dispatchPostOrder: orderActions.postOrder,
  dispatchDeleteMessage: appActions.deleteErrorMessage,
  dispatchUpdateOrder: orderActions.updateOrder,
  dispatchGetOrder: orderActions.getOrder,
};

export default connect(mapStateProps, mapDispatchToProps)(OrderForm);
