import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import App from './components/App';
import registerServiceWorker from './registerServiceWorker';
import { store } from './store';
import { loginActions } from './actions/index';

const token = localStorage.getItem('token');

axios.post('/auth/verifyToken', { token })
  .then((response) => {
    if (!response.data.isValid) {
      localStorage.removeItem('token');
    } else {
      axios.defaults.headers.common.Authorization = `Bearer ${token}`;
      store.dispatch(loginActions.loginUser());
    }
  }).then(() => {
    ReactDOM.render(<App />, document.getElementById('root'));
  });
registerServiceWorker();
