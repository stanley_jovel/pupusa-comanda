import { loginTypes } from '../actionTypes';

const initialState = {
  isLoggedIn: false,
};
export const login = (state = initialState, action) => {
  switch (action.type) {
    case loginTypes.LOGGED_TRUE:
      return {
        ...state,
        isLoggedIn: true,
      };
    case loginTypes.LOGGED_FALSE:
      return {
        ...state,
        isLoggedIn: false,
      };
    default:
      return state;
  }
};

