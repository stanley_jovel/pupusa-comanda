import { orderTypes } from '../actionTypes';

export const orders = (state = {}, action) => {
  switch (action.type) {
    // case orderTypes.ADD_ORDER:
    case orderTypes.SUCCESS_GET_ORDER:
      return {
        ...state,
        [action.payload._id]: {
          ...state[action.payload._id],
          ...action.payload,
          isLoaded: true,
          isLoading: false,
        },
      };
    case orderTypes.SUCCESS_GET_ORDERS:
      return action.payload;
    case orderTypes.PENDING_GET_ORDER:
      return {
        ...state,
        [action.payload]: {
          ...state[action.payload],
          _id: action.payload,
          isLoading: true,
          isLoaded: false,
        },
      };
    case orderTypes.SUCCESS_DELETE_ORDER: {
      const newState = { ...state };
      delete newState[action.payload._id];
      return newState;
    }
    case orderTypes.FAILED_GET_ORDERS:
      return state;
    case orderTypes.NO_ORDER_RESULTS:
      return state;
    default:
      return state;
  }
};

