import { orderTypes, appTypes } from '../actionTypes';

const initialState = {
  errorMessage: '',
};

export const app = (state = initialState, action) => {
  switch (action.type) {
    case orderTypes.FAILED_POST_ORDERS:
    case orderTypes.FAILED_UPDATE_ORDERS:
      return {
        ...state,
        errorMessage: action
          .payload
          .response
          .data
          .message,
      };
    case appTypes.DELETE_ERROR_MESSAGE:
      return {
        ...state,
        errorMessage: '',
      };
    default:
      return state;
  }
};

