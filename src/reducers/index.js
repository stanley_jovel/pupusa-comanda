import { combineReducers } from 'redux';

import { orders } from './orders';
import { app } from './app';
import { login } from './login';

export const rootReducer = combineReducers({
  orders,
  app,
  login,
});
