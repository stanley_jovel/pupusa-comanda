import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const SlatOuter = styled.section`
  margin: auto;
  overflow: hidden;
  font-family: ${({ theme }) => theme.baseFont};
  font-weight: normal;
  width: 100%;
  @media (min-width: ${({ theme }) => theme.xlarge.inner}) {
    max-width: ${props => props.theme.xlarge.outer};
  };
  background-color: ${({ theme }) => theme.brownPaperBag};
  background-image: url(${({ bgImage }) => bgImage || '/images/craft-paper.png'});
  background-repeat: repeat;
`;

export const SlatInner = styled.div`
  margin: auto;
  overflow: hidden;
  padding: 40px 0;
  @media (min-width: ${({ theme }) => theme.large.start}) {
    width: ${({ largeWidth }) => largeWidth || 100}%;
    max-width: ${({ theme }) => theme.large.inner};
  };
  @media (min-width: ${({ theme }) => theme.xlarge.start}) {
    width: ${({ xLargeWidth }) => xLargeWidth || 100}%;
    max-width: ${({ theme }) => theme.xlarge.inner};
  };
`;

export const LinkButton = styled(Link)`
  background-color: ${({ theme }) => theme.brown};
  background-image: none;
  background-position: 20px center;
  background-repeat: no-repeat;
  background-size: 15px 14px;
  border-radius: 50px;
  box-shadow: none;
  box-sizing: border-box;
  color: ${({ theme }) => theme.brown};
  display: inline-block;
  font-size: 14px;
  font-weight: 600;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.21;
  letter-spacing: 2.8px;
  text-align: center;
  color: ${({ theme }) => theme.white};
  letter-spacing: 1.5px;
  padding: 10px 15px 10px 15px;
  text-align: center;
  text-transform: uppercase;
  width: fit-content;
  height: 35px;
  text-decoration: none;
  &:hover {
    box-shadow: inset 0px 0px 0px 2px ${({ theme }) => theme.brown};
    background-color: ${({ theme }) => theme.transparent};
    color: ${({ theme }) => theme.brown};
  };
`;

export const LinkButtonWrapper = styled.div`
 display: flex;
 justify-content: ${({ alignment }) => (alignment ? 'flex-start' : 'flex-end')};
`;
