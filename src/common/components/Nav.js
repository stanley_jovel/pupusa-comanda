import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { LinkButton, LinkButtonWrapper } from './';
import { loginActions } from '../../actions';

class NavComponent extends React.Component {
  static propTypes = {
    isLoggedIn: PropTypes.bool.isRequired,
  }

  logOut = () => {
    localStorage.removeItem('token');
    this.dispatchLogoutUser();
  }

  render() {
    return this.props.isLoggedIn ?
      (
        <LinkButtonWrapper align="right">
          <LinkButton to="/login" onClick={this.logOut}>Logout</LinkButton>
        </LinkButtonWrapper>
      )
      : null;
  }
}

const mapStateToProps = state => ({
  isLoggedIn: state.login.isLoggedIn,
});

const mapDispatchToProps = {
  dispatchLogoutUser: loginActions.logoutUser,
};

export const Nav = connect(mapStateToProps, mapDispatchToProps)(NavComponent);

