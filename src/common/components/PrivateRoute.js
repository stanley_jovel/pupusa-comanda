import React from 'react';
import { Route, Redirect } from 'react-router';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

const PrivateRouteComponent = ({
  ...rest,
  component: Component,
  isLoggedIn,
}) => (
  <Route
    {...rest}
    component={props => (
      isLoggedIn
      ? <Component {...props} />
      : <Redirect to={{
        pathname: '/login',
        state: { from: props.location }, /* eslint-disable-line */
      }}
      />
    )}
  />
);

PrivateRouteComponent.propTypes = {
  component: PropTypes.func.isRequired,
  isLoggedIn: PropTypes.bool.isRequired,
};

const mapStateToProps = state => ({
  isLoggedIn: state.login.isLoggedIn,
});

export const PrivateRoute = connect(mapStateToProps, null)(PrivateRouteComponent);

