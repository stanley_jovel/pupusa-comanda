import axios from 'axios';

import { loginTypes } from '../actionTypes';

export const loginUser = payload => ({
  type: loginTypes.LOGGED_TRUE,
  payload,
});

export const logoutUser = payload => ({
  type: loginTypes.LOGGED_FALSE,
  payload,
});

export const failedGetAuth = payload => ({
  type: loginTypes.FAILED_GET_AUTH,
  payload,
});

export const failedPostAuth = payload => ({
  type: loginTypes.FAILED_POST_AUTH,
  payload,
});


export const postAuth = loginParams => (dispatch) => {
  axios.post('auth/login', {
    username: loginParams.username,
    password: loginParams.password,
  })
    .then((response) => {
      axios.defaults.headers.common.Authorization = `Bearer ${response.data.token}`;
      localStorage.setItem('token', response.data.token);
      dispatch(loginUser());
    })
    .catch((error) => {
      dispatch(failedPostAuth(error));
    });
};
