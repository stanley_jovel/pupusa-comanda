import axios from 'axios';
import _ from 'lodash';

import { orderTypes } from '../actionTypes';

export const addOrder = payload => ({
  type: orderTypes.ADD_ORDER,
  payload,
});

export const successGetOrders = payload => ({
  type: orderTypes.SUCCESS_GET_ORDERS,
  payload,
});

export const successGetOrder = payload => ({
  type: orderTypes.SUCCESS_GET_ORDER,
  payload,
});

export const failedGetOrders = payload => ({
  type: orderTypes.FAILED_GET_ORDERS,
  payload,
});

export const failedPostOrders = payload => ({
  type: orderTypes.FAILED_POST_ORDERS,
  payload,
});

export const failedUpdateOrders = payload => ({
  type: orderTypes.FAILED_UPDATE_ORDERS,
  payload,
});


export const ordersNotResults = () => ({
  type: orderTypes.NO_ORDER_RESULTS,
});

export const getOrders = () => (dispatch, getState) => {
  const currentOrders = getState().orders;
  if (_.size(currentOrders) === 0) {
    axios.get('/orders')
      .then((response) => {
        const orders = _.keyBy(response.data, '_id');
        dispatch(successGetOrders(orders));
      })
      .catch((error) => {
        dispatch(failedGetOrders(error));
      });
  } else {
    dispatch(ordersNotResults());
  }
};

export const postOrder = (orderParams, push) => (dispatch) => {
  axios.post('/orders', orderParams)
    .then((response) => {
      const newOrder = response.data;
      dispatch(addOrder(newOrder));
      push('/');
    })
    .catch((error) => {
      dispatch(failedPostOrders(error));
    });
};

export const updateOrder = (orderParams, push) => (dispatch) => {
  axios.put(`/orders/${orderParams._id}`, orderParams)
    .then((response) => {
      const updatedOrder = response.data;
      dispatch(addOrder(updatedOrder));
      push('/');
    })
    .catch((error) => {
      dispatch(failedUpdateOrders(error));
    });
};
export const deleteOrder = orderId => (dispatch) => {
  axios.delete(`/orders/${orderId}`)
    .then((response) => {
      const deletedOrder = response.data;
      dispatch({
        type: orderTypes.SUCCESS_DELETE_ORDER,
        payload: deletedOrder,
      });
    })
    .catch((error) => {
      dispatch({
        type: orderTypes.FAILED_DELETE_ORDERS,
        payload: error,
      });
    });
};


export const getOrder = orderId => (dispatch, getState) => {
  const currentOrders = getState().orders;
  const order = currentOrders[orderId];

  const isLoading = _.get(order, 'isLoading', false);
  const isLoaded = _.get(order, 'isLoaded', false);

  if (!isLoading && !isLoaded) {
    dispatch({
      type: orderTypes.PENDING_GET_ORDER,
      payload: orderId,
    });
    return axios.get(`/orders/${orderId}`)
      .then((response) => {
        dispatch(successGetOrder(response.data));

        return {
          isPending: false,
        };
      })
      .catch((error) => {
        dispatch(failedGetOrders(error));
      });
  }
  return Promise.resolve({ isPending: true });
};
