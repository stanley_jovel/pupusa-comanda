import { appTypes } from '../actionTypes/';

export const deleteErrorMessage = () => ({
  type: appTypes.DELETE_ERROR_MESSAGE,
});

