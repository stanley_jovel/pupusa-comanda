export const GET_AUTH = 'GET_AUTH';
export const FAILED_POST_AUTH = 'FAILED_POST_AUTH';
export const FAILED_GET_AUTH = 'FAILED_GET_AUTH';

export const LOGGED_TRUE = 'LOGGED_TRUE';
export const LOGGED_FALSE = 'LOGGED_FALSE';

